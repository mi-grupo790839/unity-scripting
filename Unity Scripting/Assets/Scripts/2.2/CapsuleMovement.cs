using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour
{
  private Vector3 direction; // posibles ejes de movimiento
    public float speed;    // velocidad de movimiento
    

    void Update()
    {
        // Acotacion de los valores de direccion al valor unitario [-1,1]
        direction = ClampVector3(direction);

        // Desplazamiento del componente transform en base al tiempo
        transform.Translate(direction * (speed * Time.deltaTime));
    }

    /*
     * Funcion auxiliar que permite acotar los valores de las componentes
     * de un Vector3
     */
     public static Vector3 ClampVector3(Vector3 target){

        float ClampedX = Mathf.Clamp(target.x, -1f, 1f);
        float ClampedY = Mathf.Clamp(target.y, -1f, 1f);
        float ClampedZ = Mathf.Clamp(target.z, -1f, 1f);

       Vector3 result = new Vector3(ClampedX, ClampedY, ClampedZ);

        return result;
     }
 }

