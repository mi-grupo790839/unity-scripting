using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFPSController3 : MonoBehaviour
{
    private CharacterMovement characterMovement;
    private MouseLook mouseLook;
   
              // Cam rotates along x axis

    // use this for initilization
    void Start() {
        // Hide and look mouse cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        //characterMovement = GetComponent<CharacterMovement>();

        if (characterMovement == null)
        {print("aaaaaaaa");}
        mouseLook = GetComponent<MouseLook>();
        if (mouseLook == null)
        {print("aaaaaaaa2");}
    }

    // Update is called once per frame
    void Update() {
      // movement();
       rotation();
    }
     private void movement()
    {
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        characterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);
    }
     private void rotation()
    {
        float hRotationInput = Input.GetAxis("Mouse X");
        float vRotationInput = Input.GetAxis("Mouse Y");

        mouseLook.handleRotation(hRotationInput, vRotationInput); 
    }
}

